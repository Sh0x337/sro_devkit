#pragma once

#define _CRT_SECURE_NO_DEPRECATE

#include <Windows.h>

#include <cassert>
#include <remodel/GlobalVar.h>
#include <remodel/GlobalPtr.h>

#include <d3d9.h>
#include <d3dx9.h>

#include "multibyte.h"

