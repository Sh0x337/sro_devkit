if not exist "build" mkdir "build"
cd build

SET PATH=%PATH%;C:\Program Files\CMake 2.8\bin\
set

call "%VS80COMNTOOLS%vsvars32.bat"

cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 8 2005" ..

msbuild /property:Configuration=Release SRO_DevKit.sln

cd ..
